import sys
import os
from PyQt5 import QtGui, QtWidgets, QtCore
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import pytesseract
import re
from PIL import Image
import cv2
import pathlib
from openpyxl import Workbook
from openpyxl import load_workbook
	
pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files (x86)\Tesseract-OCR\tesseract.exe"

class Ui_MainWindow(QMainWindow):
	def __init__(self):
		super(Ui_MainWindow,self).__init__()
		self.setGeometry(50,50,800,500)
		self.setWindowTitle ("Power Meter Video Analyzer (PMVA)")
		self.setWindowIcon(QtGui.QIcon("bike.png"))
		self.style_fusion()
		font = QFont("Arial")
		fontSize = 12
		font.setPointSize(fontSize)
		font.setStyleHint(QFont.Monospace)
		QApplication.setFont(font)
		self.setupUi(self)

		
	def setupUi(self, MainWindow):
		MainWindow.setObjectName("Power Meter Video Analyzer (PMVA)")
		MainWindow.resize(789, 650)
		self.centralwidget = QtWidgets.QWidget(MainWindow)
		self.centralwidget.setObjectName("centralwidget")
		self.gridLayout_3 = QtWidgets.QGridLayout(self.centralwidget)
		self.gridLayout_3.setObjectName("gridLayout_3")
		spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
		self.gridLayout_3.addItem(spacerItem, 1, 0, 1, 1)
		spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
		self.gridLayout_3.addItem(spacerItem1, 2, 1, 1, 1)
		spacerItem2 = QtWidgets.QSpacerItem(30, 30, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
		self.gridLayout_3.addItem(spacerItem2, 1, 2, 1, 1)
		spacerItem3 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
		self.gridLayout_3.addItem(spacerItem3, 0, 1, 1, 1)
		self.gridLayout = QtWidgets.QGridLayout()
		self.gridLayout.setObjectName("gridLayout")
		
		self.label_Subject = QtWidgets.QLabel(self.centralwidget)
		self.label_Subject.setObjectName("label_Subject")
		self.gridLayout.addWidget(self.label_Subject, 0, 0, 1, 1, QtCore.Qt.AlignRight)
		self.lineEdit_Subject = QtWidgets.QLineEdit(self.centralwidget)
		self.lineEdit_Subject.setObjectName("lineEdit_Subject")
		self.gridLayout.addWidget(self.lineEdit_Subject, 0, 1, 1, 1)
		
		self.label_Experiment = QtWidgets.QLabel(self.centralwidget)
		self.label_Experiment.setObjectName("label_Experiment")
		self.gridLayout.addWidget(self.label_Experiment, 1, 0, 1, 1, QtCore.Qt.AlignRight)
		self.lineEdit_Experiment = QtWidgets.QLineEdit(self.centralwidget)
		self.lineEdit_Experiment.setObjectName("lineEdit_Experiment")
		self.gridLayout.addWidget(self.lineEdit_Experiment, 1, 1, 1, 1)
		
		self.label_Trial = QtWidgets.QLabel(self.centralwidget)
		self.label_Trial.setObjectName("label_Trial")
		self.gridLayout.addWidget(self.label_Trial, 2, 0, 1, 1, QtCore.Qt.AlignRight)
		self.spinBox_Trial = QtWidgets.QSpinBox(self.centralwidget)
		self.spinBox_Trial.setObjectName("spinBox_Trial")
		self.gridLayout.addWidget(self.spinBox_Trial, 2, 1, 1, 1)
		
		self.label__Date = QtWidgets.QLabel(self.centralwidget)
		self.label__Date.setObjectName("label__Date")
		self.gridLayout.addWidget(self.label__Date, 3, 0, 1, 1, QtCore.Qt.AlignRight)
		startDate = QDate.currentDate()
		self.dateEdit = QtWidgets.QDateEdit(startDate,self.centralwidget)
		self.dateEdit.setObjectName("dateEdit")
		self.gridLayout.addWidget(self.dateEdit, 3, 1, 1, 1)
		
		self.label_Time = QtWidgets.QLabel(self.centralwidget)
		self.label_Time.setObjectName("label_Time")
		self.gridLayout.addWidget(self.label_Time, 4, 0, 1, 1, QtCore.Qt.AlignRight)
		startTime = QTime.currentTime()
		self.timeEdit = QtWidgets.QTimeEdit(startTime,self.centralwidget)
		self.timeEdit.setObjectName("timeEdit")
		self.gridLayout.addWidget(self.timeEdit, 4, 1, 1, 1)
		
		self.label_Power_Meter = QtWidgets.QLabel(self.centralwidget)
		self.label_Power_Meter.setObjectName("label_Power_Meter")
		self.gridLayout.addWidget(self.label_Power_Meter, 5, 0, 1, 1, QtCore.Qt.AlignRight)
		self.comboBox_Power_Meter = QtWidgets.QComboBox(self.centralwidget)
		self.comboBox_Power_Meter.setObjectName("comboBox_Power_Meter")
		self.comboBox_Power_Meter.addItem("")
		self.gridLayout.addWidget(self.comboBox_Power_Meter, 5, 1, 1, 1)
		
		self.label_Software = QtWidgets.QLabel(self.centralwidget)
		self.label_Software.setObjectName("label_Software")
		self.gridLayout.addWidget(self.label_Software, 6, 0, 1, 1, QtCore.Qt.AlignRight)
		self.comboBox_Software = QtWidgets.QComboBox(self.centralwidget)
		self.comboBox_Software.setObjectName("comboBox_Software")
		self.comboBox_Software.addItem("")
		self.gridLayout.addWidget(self.comboBox_Software, 6, 1, 1, 1)
		
		self.label_Logs = QtWidgets.QLabel(self.centralwidget)
		self.label_Logs.setObjectName("label_Logs")
		self.gridLayout.addWidget(self.label_Logs, 8, 0, 1, 1, QtCore.Qt.AlignRight|QtCore.Qt.AlignTop)
		self.textBrowser = QtWidgets.QTextBrowser(self.centralwidget)
		self.textBrowser.setObjectName("textBrowser")
		self.textBrowser.append("Welcome to Power Meter Video Analyzer (PMVA).\n\nPlease enter the above information and choose which tool you would like to use.")
		self.gridLayout.addWidget(self.textBrowser, 8, 1, 1, 1)
		
		self.label_Progress = QtWidgets.QLabel(self.centralwidget)
		self.label_Progress.setObjectName("label_Progress")
		self.gridLayout.addWidget(self.label_Progress, 9, 0, 1, 1, QtCore.Qt.AlignRight)
		self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
		self.progressBar.setProperty("value", 0)
		self.progressBar.setObjectName("progressBar")
		self.gridLayout.addWidget(self.progressBar, 9, 1, 1, 1)
		
		self.gridLayout_3.addLayout(self.gridLayout, 1, 1, 1, 1)
		MainWindow.setCentralWidget(self.centralwidget)
		
		#menubar
		self.menubar = QtWidgets.QMenuBar(MainWindow)
		self.menubar.setGeometry(QtCore.QRect(0, 0, 789, 21))
		self.menubar.setObjectName("menubar")
		self.menuFile = QtWidgets.QMenu(self.menubar)
		self.menuFile.setObjectName("menuFile")
		self.menuView = QtWidgets.QMenu(self.menubar)
		self.menuView.setObjectName("menuView")
		self.menuFont = QtWidgets.QMenu(self.menuView)
		self.menuFont.setObjectName("menuFont")
		self.menuTools = QtWidgets.QMenu(self.menubar)
		self.menuTools.setObjectName("menuTools")
		MainWindow.setMenuBar(self.menubar)
		self.statusbar = QtWidgets.QStatusBar(MainWindow)
		self.statusbar.setObjectName("statusbar")
		MainWindow.setStatusBar(self.statusbar)
		self.actionExit_2 = QtWidgets.QAction(MainWindow)
		self.actionExit_2.setObjectName("actionExit_2")
		self.actionFullscreen = QtWidgets.QAction(MainWindow)
		self.actionFullscreen.setObjectName("actionFullscreen")
		self.actionVideo_to_Text = QtWidgets.QAction(MainWindow)
		self.actionVideo_to_Text.setObjectName("actionVideo_to_Text")
		self.actionVideo_to_Images = QtWidgets.QAction(MainWindow)
		self.actionVideo_to_Images.setObjectName("actionVideo_to_Images")
		self.actionImages_to_Text = QtWidgets.QAction(MainWindow)
		self.actionImages_to_Text.setObjectName("actionImages_to_Text")
		self.actionSettings = QtWidgets.QAction(MainWindow)
		self.menuFile.addAction(self.actionExit_2)
		self.menuView.addAction(self.actionFullscreen)
		self.menuTools.addAction(self.actionVideo_to_Text)
		self.menuTools.addAction(self.actionVideo_to_Images)
		self.menuTools.addAction(self.actionImages_to_Text)
		self.menubar.addAction(self.menuFile.menuAction())
		self.menubar.addAction(self.menuView.menuAction())
		self.menubar.addAction(self.menuTools.menuAction())
		
		#toolbar
		self.toolEverything = QAction(QIcon("bike.png"), "Video to Data Tool", self)
		self.toolEverything.triggered.connect(self.video_to_data)
		self.toolVideo = QAction(QIcon("Video.png"), "Video to Image Tool", self)
		self.toolVideo.triggered.connect(self.video_to_images)
		self.toolImages = QAction(QIcon("Image.png"), "Images to Data Tool", self)
		self.toolImages.triggered.connect(self.images_to_data)
		self.toolBar = self.addToolBar("Toolbar")
		self.toolBar.addAction(self.toolEverything)
		self.toolBar.addAction(self.toolVideo)
		self.toolBar.addAction(self.toolImages)

		
		self.retranslateUi(MainWindow)
		QtCore.QMetaObject.connectSlotsByName(MainWindow)

	def retranslateUi(self, MainWindow):
		_translate = QtCore.QCoreApplication.translate
		MainWindow.setWindowTitle(_translate("MainWindow", "Power Meter Video Analyzer (PMVA)"))
		self.label_Progress.setText(_translate("MainWindow", "Progress:"))
		self.comboBox_Software.setItemText(0, _translate("MainWindow", "Qalvin Legacy - Desktop"))
		self.label_Trial.setText(_translate("MainWindow", "Trial:"))
		self.label_Power_Meter.setText(_translate("MainWindow", "Power Meter:"))
		self.label_Time.setText(_translate("MainWindow", "Time:"))
		self.label_Subject.setText(_translate("MainWindow", "Subject:"))
		self.label_Experiment.setText(_translate("MainWindow", "Experiment:"))
		self.label__Date.setText(_translate("MainWindow", "Date:"))
		self.label_Software.setText(_translate("MainWindow", "Software:"))
		self.comboBox_Power_Meter.setItemText(0, _translate("MainWindow", "Quarq\'s Power Meter"))
		self.label_Logs.setText(_translate("MainWindow", "Log:"))
		self.menuFile.setTitle(_translate("MainWindow", "File"))
		self.menuView.setTitle(_translate("MainWindow", "View"))
		self.menuTools.setTitle(_translate("MainWindow", "Tools"))
		self.actionExit_2.setText(_translate("MainWindow", "Exit"))
		self.actionExit_2.setShortcut("Esc")
		self.actionExit_2.setStatusTip("Leaves the Application.")
		self.actionFullscreen.setText(_translate("MainWindow", "Fullscreen"))
		self.actionFullscreen.setShortcut("F11")
		self.actionFullscreen.setStatusTip("Switch Between Fullscreen and Windowed Modes.")
		self.actionVideo_to_Text.setText(_translate("MainWindow", "Video to Text"))
		self.actionVideo_to_Text.setShortcut("F1")
		self.actionVideo_to_Text.setStatusTip("Creates data in XLSX format from a selected video file.")
		self.actionVideo_to_Images.setText(_translate("MainWindow", "Video to Images"))
		self.actionVideo_to_Images.setShortcut("F2")
		self.actionVideo_to_Images.setStatusTip("Creates images in JPG format from a selected video file.")
		self.actionImages_to_Text.setText(_translate("MainWindow", "Images to Text"))
		self.actionImages_to_Text.setShortcut("F3")
		self.actionImages_to_Text.setStatusTip("Creates data in XLSX format from a selected group of image files.")
		self.actionSettings.setText(_translate("MainWindow", "Settings"))
		
		#MENU EVENTS
		self.actionExit_2.triggered.connect(self.close_application)
		self.actionFullscreen.triggered.connect(self.fullscreen)
		self.actionVideo_to_Text.triggered.connect(self.video_to_data)
		self.actionVideo_to_Images.triggered.connect(self.video_to_images)
		self.actionImages_to_Text.triggered.connect(self.images_to_data)
		
		
	def video_to_data(self):
		#check states
		subject_name = False
		experiment_name = False
		if window.lineEdit_Subject.text() != "":
			subject_name = True
		else:
			window.textBrowser.append("Please enter a subject name.")
			
		if window.lineEdit_Experiment.text() != "":
			experiment_name = True
		else:
			window.textBrowser.append("Please enter an experiment name.")
		
		#run program
		if subject_name == True & experiment_name == True:
		
			#don't allow changes
			window.lineEdit_Subject.setReadOnly(True)
			window.lineEdit_Experiment.setReadOnly(True)
			window.spinBox_Trial.setReadOnly(True)
			window.dateEdit.setReadOnly(True)
			window.timeEdit.setReadOnly(True)
			window.comboBox_Power_Meter.setEnabled(False)
			window.comboBox_Software.setEnabled(False)
			window.toolEverything.setEnabled(False)
			window.toolVideo.setEnabled(False)
			window.toolImages.setEnabled(False)
			window.menubar.setEnabled(False)
		
			calcs = Calculations()
			calcs.video_to_data()
	
	def video_to_images(self):
		window.textBrowser.append("Feature not currently supported.")
		
	def images_to_data(self):
		window.textBrowser.append("Feature not currently supported.")
		
	def style_fusion(self):
		QApplication.setStyle(QStyleFactory.create("fusion"))
		

	def fullscreen(self):
		if self.isMaximized() == False:
			self.showMaximized()
		else:
			self.showNormal()
			
	def close_application(self):
		choice = QMessageBox.question(self, "Exiting", "Are you sure you want to exit?", QMessageBox.Yes | QMessageBox.No)
		if choice == QMessageBox.Yes:
			sys.exit()
		else:
			pass
		
		
	def closeEvent(self, event):
		event.ignore()
		self.close_application()
		
		
		
		
class Calculations():
	def __init__(self):
		self.videoname = pathlib.PureWindowsPath("")
		self.imageDir = pathlib.PureWindowsPath("")
		self.excelDir = pathlib.PureWindowsPath("")
		self.totalImages = 0
		self.completed = 0
		self.cancelled = False
		
	def define_video_dir(self):
		window.textBrowser.append("\nPlease locate your video file.")
		name1_temp = QFileDialog.getOpenFileName(window, "Select Your Video File")
		name1 = str(name1_temp)
		c1 = name1.find("(") + 2
		c2 = name1.find(",") - 1
		self.videoname = pathlib.PureWindowsPath(name1[c1:c2])
		
		if name1[c1:c2] != "":
			self.define_image_dir()
		else:
			self.cancelled = True
			window.textBrowser.append("Activity has be cancelled.")
	
	def define_image_dir(self):
		if self.cancelled == False:
			window.textBrowser.append("Please locate your image folder.")
			name2_temp = QFileDialog.getExistingDirectory(window,"Select Your Image Folder")
			name2 = str(name2_temp)
			
			if name2:
				self.imageDir = pathlib.PureWindowsPath(name2)
				self.define_excel_dir()
			else:
				self.cancelled = True
				window.textBrowser.append("Activity has be cancelled.")
	
	def define_excel_dir(self):
		if self.cancelled == False:
			window.textBrowser.append("Please locate your Excel folder.")
			name3_temp = QFileDialog.getExistingDirectory(window,"Select Your XLSX Folder")
			name3 = str(name3_temp)
			
			if name3:
				dash = r"\ "
				dash = dash.strip()
				self.excelDir = pathlib.PureWindowsPath(name3)
				tempName = str(self.excelDir) + dash + str(window.lineEdit_Experiment.text()) + ".xlsx"
				self.excelDir = tempName
				
				if os.path.isfile(self.excelDir) == False:
					wb = Workbook()
					subject = window.lineEdit_Subject.text()
					ws = wb.active
					ws.title = subject
					ws['A1'] = "Trial"
					ws['B1'] = "Date"
					ws['C1'] = "Time" 
					ws['D1'] = "Power Meter"
					ws['E1'] = "Software"
					ws['F1'] = "Dataset"
					wb.save(self.excelDir)
				else:
					wb = load_workbook(self.excelDir)
					subject = window.lineEdit_Subject.text()
					subjectExists = False
					for sheet in wb:
						if sheet.title == subject:
							subjectExists = True
					if subjectExists == False:
						ws = wb.create_sheet(subject)
						ws['A1'] = "Trial"
						ws['B1'] = "Date"
						ws['C1'] = "Time" 
						ws['D1'] = "Power Meter"
						ws['E1'] = "Software"
						ws['F1'] = "Dataset"
			else:
				self.cancelled = True
				window.textBrowser.append("Activity has be cancelled.")
	
	
	def video_to_data(self):
		#select video file
		self.define_video_dir()
		
		if self.cancelled == False:
			#run video analysis
			window.textBrowser.append("Transforming Video to Images...")
			self.video_analysis()
		
		if self.cancelled == False:
			#run image analysis
			window.textBrowser.append("Transforming Images to Text...")
			window.textBrowser.append("DO NOT OPEN THE XLSX FILE TO WHICH YOU ARE WRITING!")
			self.image_analysis()
		
		#allow changes at the end
		window.lineEdit_Subject.setReadOnly(False)
		window.lineEdit_Experiment.setReadOnly(False)
		window.spinBox_Trial.setReadOnly(False)
		window.dateEdit.setReadOnly(False)
		window.timeEdit.setReadOnly(False)
		window.comboBox_Power_Meter.setEnabled(True)
		window.comboBox_Software.setEnabled(True)
		window.toolEverything.setEnabled(True)
		window.toolVideo.setEnabled(True)
		window.toolImages.setEnabled(True)
		window.menubar.setEnabled(True)
		
	
	def video_to_images(self):
		pass
		
	def images_to_data(self):
		pass
		
		
	def video_analysis(self):

		vidcap = cv2.VideoCapture(str(self.videoname))
		success,photo = vidcap.read()
		count = 0
		while success == True:
			photoname1 = r"/photo" + str(count) + ".jpg"
			cv2.imwrite(str(self.imageDir)+photoname1,photo)     # save frame as JPEG file
			success,photo = vidcap.read()
			count += 1
			length = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))

			#Progress Bar Live Update
			self.completed = int(count/length*100)
			window.progressBar.setValue(self.completed)
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
			if success == False:
				self.totalImages = count
				break
		vidcap.release()
		cv2.destroyAllWindows() 
		window.textBrowser.append("Your video file has been converted to image files.")
		
		
		
	def image_analysis(self):
		total = self.totalImages
		length_of_total = range(total)
		self.totalImages = 0
		imagesavailable = False
		dataset_count = 0
		
		dataset = []
		if total > 0:
			imagesavailable = True
			for i in length_of_total:
				filename = str(self.imageDir) + r"\photo" + str(i) + ".jpg"
				img = cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
				ret, mask = cv2.threshold(img, 150, 255, cv2.THRESH_BINARY_INV)
				h, w = img.shape
				roi = mask[int(h*0.85):int(h*0.95), int(w*0.42):int(w*0.62)]
				t1 = pytesseract.image_to_string(roi)
				c1 = t1.find('(') + 1
				c2 = t1.find(')')
				t2 = t1[c1:c2]
					
					
				if dataset_count > 1:
					if dataset[dataset_count-1] != t2:
						dataset.append("")
						dataset[dataset_count] = t2
						dataset_count += 1
				if dataset_count == 1:
					if dataset[0] != t2:
						dataset.append("")
						dataset[dataset_count] = t2
						dataset_count += 1
				if i == 0:
					dataset.append("")
					dataset[0] = t2
					dataset_count += 1	
						
				
				#Progress Bar Live Update
				self.completed = int((i+1)/total*100)
				window.progressBar.setValue(self.completed)
				if cv2.waitKey(1) & 0xFF == ord('q'):
					break
				
				cv2.destroyAllWindows() 
					
		window.textBrowser.append("Images have been processed.")
			
		if imagesavailable == True:
			#Write the data to the excel file
			#load workbook
			filelocation = self.excelDir
			wb = load_workbook(filelocation)
			
			#Get Trial Info from Text Boxes
			subject = window.lineEdit_Subject.text()
			trial_temp = window.spinBox_Trial.value()
			date_temp = window.dateEdit.date()
			time_temp = window.timeEdit.time()
			powermeter = window.comboBox_Power_Meter.currentText()
			software = window.comboBox_Software.currentText()
			
			#Convert all non-string variables to a strings
			time = time_temp.toString()
			date = date_temp.toString()
			trial = str(trial_temp)
			
			#setup for worksheet
			subjectExists = False
			for sheet in wb:
				if sheet.title == subject:
					subjectExists = True
			if subjectExists == False:
				ws = wb.create_sheet(subject)
				ws['A1'] = "Trial"
				ws['B1'] = "Date"
				ws['C1'] = "Time" 
				ws['D1'] = "Power Meter"
				ws['E1'] = "Software"
				ws['F1'] = "Dataset"
			else:
				ws = wb[subject]
				
			# New trial informational data adding:
			rowmax = ws.max_row + 1
			ws.cell(row=rowmax,column=1).value = int(trial)
			ws.cell(row=rowmax,column=2).value = date
			ws.cell(row=rowmax,column=3).value = time
			ws.cell(row=rowmax,column=4).value = powermeter
			ws.cell(row=rowmax,column=5).value = software
			
			#adding torque data
			dataset_range = range(dataset_count)
			for i in dataset_range:
				ws.cell(row=rowmax,column=i+6).value = float(dataset[i])

				
			wb.save(filename = filelocation)
			window.textBrowser.append("\nYour Excel file has been updated.\nYou may now open your XLSX file.")
		else:
			window.textBrowser.append("\nNo images were found in the target folder.")
			
			
			

if __name__=="__main__":
	app = QApplication(sys.argv)
	window = Ui_MainWindow()
	window.show()
	sys.exit(app.exec_())
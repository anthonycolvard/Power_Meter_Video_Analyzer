import sys
from cx_Freeze import *

# Dependencies are automatically detected, but it might need fine tuning.

includefiles = ['bike.ico','bike.png','Image.png','Video.png','Files/']
includes = ["atexit","PyQt5"]
includefiles = [r"C:\Users\HumanPerfResLab04\AppData\Local\Programs\Python\Python35\Lib\site-packages\PyQt5\Qt\plugins\platforms\qwindows.dll"]
excludes = [
    '_gtkagg', '_tkagg', 'bsddb', 'curses', 'email', 'pywin.debugger',
    'pywin.debugger.dbgcon', 'pywin.dialogs', 'tcl',
    'Tkconstants', 'Tkinter'
]
packages = ['os','sys','numpy']
path = []

build_exe_options = {
                     "includes":      includes, 
                     "include_files": includefiles,
                     "excludes":      excludes, 
                     "packages":      packages, 
                     "path":          path
}

base = None
exe = None

if sys.platform == "win32":
    base = "Win32GUI"

if sys.platform == "win32":
    exe = Executable(
      script="D:\\imi\\aptanaWorKPCworkspace\\azhtel\\tel.py",
      initScript = None,
      base="Win32GUI",
      targetName="PMVA.exe",
      icon = 'bike.ico'
    )
	
setup(  name = "PMVA",
        version = "0.2",
        author = 'Anthony Colvard',
        description = "Power Meter Video Analyzer",
        options = {"build_exe": build_exe_options},
        executables = [Executable("PMVA.py", base=base,icon='bike.ico')])


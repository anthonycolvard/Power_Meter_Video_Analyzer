# Power Meter Video Analyzer

<h4><i>Using the video capture feature built into Windows 10, screen capture torque data form you Quarq power meter with Qalvin Legacy software and export to Excel.</i></h4>

<h3><b>INSTALLATION: </b></h3>
<p>Be sure to install Google's tesseract OCR application before installing this application. The image-to-text technology included in tesseract is vital to normal function of the PMVA application. An 'exe' file has been included for the installation of tesseract in the 'installers' folder, but you can also find this installation file at 'https://github.com/tesseract-ocr/tesseract/wiki/4.0-with-LSTM#400-alpha-for-windows'. After installing tesseract OCR, install PMVA using the included 'msi' file found in the 'Installers' folder. Note that I do not have a windows applicatino certificate, so you will have to tell windows to trust the application before installing.

<p><b>After installation:</b> Be sure to allow administrative privilages to the PMVA exe. Do this by navigating to the exicutable and right clicking the file. Then, select properties and select the the compatability tab. Check the box titled 'Run this application as an administator.' If you wish to use this application with other users on the same system, use the setting for all users.</p>

<h3><b>SECURITY & Risk:<b><h3>
<p>Unfortunitely, it is common for some developers to include molicious software with applicaitons. To allow for increased trust between consumers and developers, I have included the source material for the python applicaiton code and the python setup sctript used to create the msi file. If you are concered about personal security, feel free to check all source code and build your own msi or exe files.</p>

<h3><b> USAGE: <b></h3>
<p><b>1. Record Torque in Qalvin Legacy:</b>
To record torque values in you power meter software, connect to your power meter and navigate to "Slope Calibration".
Once slope calibration has begun, press the windows button + 'g'. This will open the game taskbar.
Press the record button, then start your trial.
Press the stop reording button when the trial has ended.</p>
<p><b>2. Accessing video files:</b>
Navigate to C:\Users\"YOURUSERNAME"\Videos\Captures or press the file icon in the windows game taskbar.
copy the appropriate video capture files [.mp4]. It is suggested that you rename them and save to your "Video_Files" folder which has been included in this release. This will help with organization.</p>
<p><b>3. Running the application:</b>
When opening the application, you should update the experiment/subject/trial/etc. information before continuing.
Then, there will be a bicycle icon on the top left of the screen. Select this icon and a file explorer will open.
First, select the video file you wish to analyze.
Second, select the folder which will hold the image files created from the video ("Image_Files" folder is suggested).
Third, select the folder which will hold the excel files created from the image files ("Excel_Files" folder is suggested).
If no excel file exists, a .xlsx file will be created with the file name being the "Experiment" string you previously entered, and the sheet name being the "Subject" string you previously entered.
If an excel file at this location exists with the same "experiment" name, the program will search for sheets with the same "Subject" name.
If the subject exists already, a new row of data will be started. Else, a new sheet for said "Subject" will be created.</p>
<p><b>4. Now we wait:</b>
The program will now create a .xlsx file or update an existing file with the data from the video you have attempted to analyze.</p>
<p><b>5. Why is this useful?:</b>
*MVC data that is ecoglogically valid to a bicycle pedal stroke is currently not accessable. This application allows for the collection of such data with limited user time commitment. The application has been accurate during pilot testing.</p>